/*
 * Copyright © 2013 Mauro Carvalho Chehab <mchehab@redhat.com>
 *
 * Permission to use, copy, modify, distribute, and sell this software
 * and its documentation for any purpose is hereby granted without
 * fee, provided that the above copyright notice appear in all copies
 * and that both that copyright notice and this permission notice
 * appear in supporting documentation, and that the name of Red Hat
 * not be used in advertising or publicity pertaining to distribution
 * of the software without specific, written prior permission.  Red
 * Hat makes no representations about the suitability of this software
 * for any purpose.  It is provided "as is" without express or implied
 * warranty.
 *
 * THE AUTHORS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN
 * NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <argp.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "kbuffer.h"
#include "event-parse.h"

/*
 * Polling time, if read() doesn't block. Currently, trace_pipe_raw never
 * blocks on read(). So, we need to sleep for a while, to avoid spending
 * too much CPU cycles. A fix for it is expected for 3.10.
 */
#define POLLING_TIME 3

/* Test for a little-endian machine */
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
	#define ENDIAN KBUFFER_ENDIAN_LITTLE
#else
	#define ENDIAN KBUFFER_ENDIAN_BIG
#endif

#define DEBUGFS "/sys/kernel/debug/"

#define ENABLE_RAS_MC_EVENT  "ras:mc_event\n"
#define DISABLE_RAS_MC_EVENT "!" ENABLE_RAS_MC_EVENT

/*
 * Tracing enable/disable code
 */
static int toggle_ras_mc_event(int enable)
{
	int fd, rc;

	/* Enable RAS events */
	fd = open(DEBUGFS "tracing/set_event", O_RDWR | O_APPEND);
	if (fd < 0) {
		perror("Open set_event");
		return errno;
	}
	if (enable)
		rc = write(fd, ENABLE_RAS_MC_EVENT,
			   sizeof(ENABLE_RAS_MC_EVENT));
	else
		rc = write(fd, DISABLE_RAS_MC_EVENT,
			   sizeof(DISABLE_RAS_MC_EVENT));
	if (rc < 0) {
		perror("can't write to set_event");
		close(fd);
		return rc;
	}
	close(fd);
	if (!rc) {
		fprintf(stderr, "nothing was written on set_event\n");
		return EIO;
	}

	if (enable)
		printf("RAS events enabled\n");
	else
		printf("RAS events disabled\n");

	return 0;
}

/*
 * Tracing read code
 */

/* Should match the code at Kernel's include/linux/edac.c */
enum hw_event_mc_err_type {
	HW_EVENT_ERR_CORRECTED,
	HW_EVENT_ERR_UNCORRECTED,
	HW_EVENT_ERR_FATAL,
	HW_EVENT_ERR_INFO,
};

static char *mc_event_error_type(unsigned long long err_type)
{
	switch (err_type) {
	case HW_EVENT_ERR_CORRECTED:
		return "Corrected";
	case HW_EVENT_ERR_UNCORRECTED:
		return "Uncorrected";
	case HW_EVENT_ERR_FATAL:
		return "Fatal";
	default:
	case HW_EVENT_ERR_INFO:
		return "Info";
	}
}

static int get_pagesize(struct pevent *pevent) {
	int fd, len, page_size = 4096;
	char buf[page_size];

	fd = open(DEBUGFS "tracing/events/header_page", O_RDONLY);
	if (fd < 0)
		return page_size;

	len = read(fd, buf, page_size);
	if (len <= 0)
		goto error;
	if (pevent_parse_header_page(pevent, buf, len, sizeof(long)))
		goto error;

	page_size = pevent->header_page_data_offset + pevent->header_page_data_size;

error:
	close(fd);
	return page_size;

}

static int ras_mc_event_handler(struct trace_seq *s,
				struct pevent_record *record,
				struct event_format *event, void *context)
{
	int len;
	unsigned long long cnt, val;
	long long top_l, mid_l, low_l;
	const char *str;

	if (pevent_get_field_val(s,  event, "error_count", record, &cnt, 0) < 0)
		return -1;
	trace_seq_printf(s, "%lld ", cnt);

	if (pevent_get_field_val(s, event, "error_type", record, &val, 1) < 0)
		return -1;
	trace_seq_puts(s, mc_event_error_type(val));
	if (cnt > 1)
		trace_seq_puts(s, " errors:");
	else
		trace_seq_puts(s, " error:");
	str = pevent_get_field_raw(s, event, "msg", record, &len, 1);
	if (!str)
		return -1;
	if (*str) {
		trace_seq_puts(s, " ");
		trace_seq_puts(s, str);
	}

	str = pevent_get_field_raw(s, event, "label", record, &len, 1);
	if (!str)
		return -1;
	if (*str) {
		trace_seq_puts(s, " on ");
		trace_seq_puts(s, str);
	}

	trace_seq_puts(s, " (");
	if (pevent_get_field_val(s,  event, "mc_index", record, &val, 0) < 0)
		return -1;
	trace_seq_printf(s, "mc: %lld", val);

	if (pevent_get_field_val(s,  event, "top_layer", record, &val, 0) < 0)
		return -1;
	top_l = (int) val;
	if (pevent_get_field_val(s,  event, "middle_layer", record, &val, 0) < 0)
		return -1;
	mid_l = (int) val;
	if (pevent_get_field_val(s,  event, "lower_layer", record, &val, 0) < 0)
		return -1;
	low_l = (int) val;

	if (top_l == 255)
		top_l = -1;
	if (mid_l == 255)
		mid_l = -1;
	if (low_l == 255)
		low_l = -1;

	if (top_l >= 0 || mid_l >= 0 || low_l >= 0) {
		if (low_l >= 0)
			trace_seq_printf(s, " location: %lld:%lld:%lld",
					top_l, mid_l, low_l);
		else if (mid_l >= 0)
			trace_seq_printf(s, " location: %lld:%lld",
					 top_l, mid_l);
		else
			trace_seq_printf(s, " location: %lld", top_l);
	}

	if (pevent_get_field_val(s,  event, "address", record, &val, 0) < 0)
		return -1;
	if (val)
		trace_seq_printf(s, " address: 0x%08llx", val);

	if (pevent_get_field_val(s,  event, "grain_bits", record, &val, 0) < 0)
		return -1;
	trace_seq_printf(s, " grain: %lld", val);


	if (pevent_get_field_val(s,  event, "syndrome", record, &val, 0) < 0)
		return -1;
	if (val)
		trace_seq_printf(s, " syndrome: 0x%08llx", val);

	str = pevent_get_field_raw(s, event, "driver_detail", record, &len, 1);
	if (!str)
		return -1;
	if (*str) {
		trace_seq_puts(s, " ");
		trace_seq_puts(s, str);
	}
	trace_seq_puts(s, ")");

	return 0;
}


static void parse_ras_data(struct pevent *pevent, struct kbuffer *kbuf,
			   void *data, unsigned long long time_stamp)
{
	struct pevent_record record;
	struct trace_seq s;

	record.ts = time_stamp;
	record.size = kbuffer_event_size(kbuf);
	record.data = data;
	record.offset = kbuffer_curr_offset(kbuf);

	/* note offset is just offset in subbuffer */
	record.missed_events = kbuffer_missed_events(kbuf);
	record.record_size = kbuffer_curr_size(kbuf);

	trace_seq_init(&s);
	pevent_print_event(pevent, &s, &record);
	trace_seq_do_printf(&s);
	printf("\n");
}

static int read_ras_event(int fd, struct pevent *pevent, struct kbuffer *kbuf,
			  void *page, int page_size)
{
	unsigned size;
	unsigned long long time_stamp;
	void *data;

	do {
		size = read(fd, page, page_size);
		if (size < 0) {
			perror ("read");
			return -1;
		} else if (size > 0) {
			kbuffer_load_subbuffer(kbuf, page);

			while ((data = kbuffer_read_event(kbuf, &time_stamp))) {
				parse_ras_data(pevent, kbuf, data, time_stamp);

				/* increment to read next event */
				kbuffer_next_event(kbuf, NULL);
			}
		} else {
			/*
			 * Before Kernel 3.10, read() never blocks. So, we
			 * need to sleep for a while
			 */
			sleep(POLLING_TIME);
		}
	} while (1);
}

static int get_num_cpus(void)
{
	int num_cpus = 0;

	DIR		*dir;
	struct dirent	*entry;

	dir = opendir(DEBUGFS "tracing/per_cpu/");
	if (!dir)
		return -1;

	for (entry = readdir(dir); entry; entry = readdir(dir)) {
		if (strstr(entry->d_name, "cpu"))
			num_cpus++;
	}
	closedir(dir);

	return num_cpus;
}

struct pthread_data {
	pthread_t	thread;
	struct pevent	*pevent;
	int		cpu;
	int		page_size;
};

static void *handle_ras_events_cpu(void *priv)
{
	int fd;
	struct kbuffer *kbuf;
	void *page;
	char pipe_raw[PATH_MAX];
	struct pthread_data *data = priv;

	page = malloc(data->page_size);
	if (!page) {
		perror("Can't allocate page");
		return NULL;
	}

	kbuf = kbuffer_alloc(KBUFFER_LSIZE_8, ENDIAN);
	if (!kbuf) {
		perror("Can't allocate kbuf");
		free(page);
		return NULL;
	}

	/* FIXME: use select to open for all CPUs */
	snprintf(pipe_raw, sizeof(pipe_raw),
		 DEBUGFS "tracing/per_cpu/cpu%d/trace_pipe_raw",
		 data->cpu);

	fd = open(pipe_raw, O_RDONLY);
	if (fd < 0) {
		perror("Can't open trace_pipe_raw");
		kbuffer_free(kbuf);
		free(page);
		return NULL;
	}

	printf("Listening to events on cpu %d\n", data->cpu);

	read_ras_event(fd, data->pevent, kbuf, page, data->page_size);

	close(fd);
	kbuffer_free(kbuf);
	free(page);

	return NULL;
}

static int handle_ras_events(void)
{
	int rc, fd, size, page_size, i, cpus;
	struct pevent *pevent;
	struct pthread_data *data;
	void *page;

	/* Enable RAS events */
	rc = toggle_ras_mc_event(1);

	pevent = pevent_alloc();
	if (!pevent) {
		perror("Can't allocate pevent");
		return errno;
	}

	fd = open(DEBUGFS "tracing/events/ras/mc_event/format",
		  O_RDONLY);
	if (fd < 0) {
		perror("Open ras format");
		rc = errno;
		goto free_pevent;
	}

	page_size = get_pagesize(pevent);

	page = malloc(page_size);
	if (!page) {
		perror("Can't allocate page to read event format");
		rc = errno;
		close(fd);
		goto free_pevent;
	}

	size = read(fd, page, page_size);
	close(fd);
	if (size < 0) {
		rc = size;
		free(page);
		goto free_pevent;
	}

	pevent_register_event_handler(pevent, -1, "ras", "mc_event",
				      ras_mc_event_handler, NULL);

	rc = pevent_parse_event(pevent, page, size, "ras");
	free(page);
	if (rc)
		goto free_pevent;

	cpus = get_num_cpus();
	data = calloc(sizeof(*data), cpus);
	if (!data)
		goto free_pevent;

	printf("Opening one thread per cpu (%d threads)\n", cpus);
	for (i = 0; i < cpus; i++) {
		data[i].pevent = pevent;
		data[i].cpu = i;
		data[i].page_size = page_size;

		rc = pthread_create(&data[i].thread, NULL,
				    handle_ras_events_cpu,
				    (void *)&data[i]);
		if (rc) {
			while (--i)
				pthread_cancel(data[i].thread);
			goto free_threads;
		}
	}

	/* Wait for all threads to complete */
	for (i = 0; i < cpus; i++)
		pthread_join(data[i].thread, NULL);

free_threads:
	free(data);

free_pevent:
	pevent_free(pevent);
	return rc;
}

/*
 * Arguments(argp) handling logic and main
 */

#define VERSION "0.1.0"
#define TOOL_NAME "edactool"
#define TOOL_DESCRIPTION "Ancillary tool to control the Error Detect And Correction - EDAC subsystem."
#define ARGS_DOC "<options>"

const char *argp_program_version = TOOL_NAME " " VERSION;
const char *argp_program_bug_address = "Mauro Carvalho Chehab <mchehab@redhat.com>";

struct arguments {
	int handle_events, enable_ras;
};

static error_t parse_opt(int k, char *arg, struct argp_state *state)
{
	struct arguments *args = state->input;

	switch (k) {
	case 'r':
		args->handle_events++;
		break;
	case 'e':
		args->enable_ras++;
		break;
	case 'd':
		args->enable_ras--;
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

int main(int argc, char *argv[])
{
	struct arguments args;
	int idx = -1;
	const struct argp_option options[] = {
		{"read",    'r', 0, 0, "read RAS events", 0},
		{"enable",  'e', 0, 0, "enable RAS events", 0},
		{"disable", 'd', 0, 0, "disable RAS events", 0},

		{ 0, 0, 0, 0, 0, 0 }
	};
	const struct argp argp = {
		.options = options,
		.parser = parse_opt,
		.doc = TOOL_DESCRIPTION,
		.args_doc = ARGS_DOC,

	};
	memset (&args, 0, sizeof(args));

	argp_parse(&argp, argc, argv, 0,  &idx, &args);

	if (idx < 0) {
		argp_help(&argp, stderr, ARGP_HELP_STD_HELP, TOOL_NAME);
		return -1;
	}

	if (args.enable_ras > 0)
		toggle_ras_mc_event(1);
	else if (args.enable_ras < 0)
		toggle_ras_mc_event(0);

	if (args.handle_events)
		handle_ras_events();

	return 0;
}
