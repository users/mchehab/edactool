CC		= gcc
CFLAGS		= -Wall
LDFLAGS		= -lpthread
FILES		= edactool
TRACE_LIB	= event-parse.o kbuffer-parse.o  parse-filter.o  parse-utils.o  trace-seq.o

default: $(FILES)

edactool: edactool.o $(TRACE_LIB)


clean:
	-rm $(FILES) $(TRACE_LIB) edactool.o
